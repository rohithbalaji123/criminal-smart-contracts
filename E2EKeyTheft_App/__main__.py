import json
import sys

from Crypto.Hash import SHA256

from E2EKeyTheft_App.Helpers import Helpers
from E2EKeyTheft_App.Services.E2EKeyTheftService import E2EKeyTheftService
from E2EKeyTheft_App.Services.FraudVerificationService import FraudVerificationService

# TODO: Get contract parameter and display here
mode = ''
try:
    mode = sys.argv[1]
except:
    print("Invalid mode")
    pass

key_theft_instance = E2EKeyTheftService.get_instance()
fraud_verification_instance = FraudVerificationService.get_instance()

if mode == "contractor":
    while True:
        print("-----------------------------------------------------------------------------------------------------------------")
        print("-----------------------------------------------------------------------------------------------------------------")
        print("Contractor options...")
        print("1. Convert message to byte array.")
        print("2. Get public key from private key.")
        print("3. Get active Secret Key Submission.")
        print("4. Report fraud of a secret key.")
        print("5. Retract reward.")
        print("6. Exit")

        option = eval(input("Enter your option : "))

        print()
        try:
            if option == 1:
                message = input("Enter message : ")
                print(Helpers.convert_string_to_byte_array(message))
            elif option == 2:
                private_key = input("Enter private key in hex : ")
                if private_key[:2] != '0x':
                    print("Invalid hex value.")
                print(Helpers.get_public_key_from_private_key(private_key))
            elif option == 3:
                print("Perpetrator Address: ", key_theft_instance.get_active_perpetrator_address_from_contract())
                print("Master Secret: ", key_theft_instance.get_active_encrypted_master_secret())
                print("Encrypted Ephemeral Private Key: ", key_theft_instance.get_active_encrypted_ephemeral_private_key())
            elif option == 4:
                master_key = input("Enter master key : ")
                ephemeral_private_key = input("Enter ephemeral private key : ")
                fraud_verification_instance.verify_fraud(bytes.fromhex(master_key), bytes.fromhex(ephemeral_private_key))
            elif option == 5:
                key_theft_instance.retract_reward_from_contract()
            elif option == 6:
                quit()
            else:
                print("Invalid option. Retry!")
        except ValueError as error:
            error = json.loads(error)
            print(error['message'])
        except Exception as exception:
            print(exception)

elif mode == "perpetrator":
    while True:
        print("-----------------------------------------------------------------------------------------------------------------")
        print("-----------------------------------------------------------------------------------------------------------------")
        print("Perpetrator options...")
        print("1. Encrypt message using contractor public key.")
        print("2. Create a commitment out of stolen keys")
        print("3. Intend to reveal.")
        print("4. Reveal key to contract.")
        print("5. Receive rewards.")
        print("6. Exit")

        option = eval(input("Enter your option : "))

        print()
        try:
            if option == 1:
                message = input("Enter message to encrypt using RSA: ")
                print("Encrypted message using rsa:", (Helpers.encrypt_message_using_rsa(bytes.fromhex(message)).hex()))
            elif option == 2:
                enc_master_secret = input("Enter encrypted master secret key: ")
                enc_ephemeral_private_key = input("Enter encrypted ephemeral secret key:" )
                print ("Commitment is: ", (key_theft_instance.create_commitment(enc_master_secret, enc_ephemeral_private_key)).hex())
            elif option == 3:
                addressIndex = eval(input("Enter the address index : "))
                commit_message = input("Enter the commit message : ")
                key_theft_instance.intend_to_reveal_key_to_contract(commit_message, addressIndex)
            elif option == 4:
                addressIndex = eval(input("Enter the address index : "))
                enc_master_key = input("Enter encrypted master key : ")
                enc_ephemeral_key = input("Enter ephemeral key : ")
                key_theft_instance.reveal_key_to_contract(enc_master_key, enc_ephemeral_key, addressIndex)
            elif option == 5:
                addressIndex = eval(input("Enter address index : "))
                key_theft_instance.get_reward_from_contract(addressIndex)
            elif option == 6:
                quit()
            else:
                print("Invalid option. Retry!")
        except ValueError as error:
            error = json.loads(format(error))
            print(error['message'])
        except Exception as exception:
            print(exception)


























# instance = E2EKeyTheftService.get_instance()
# ephemeral_private_key_string = open("E2EKeyTheft_App/Services/ecc_private_key.pem", "r").read()
# enc_masterkey = Helpers.encrypt_message_using_rsa(b'55623A501F228664BE93191A4FAFBC4670B09D45FCAA290200270EF26CD4B576')
# enc_ephemeral_private_key = Helpers.encrypt_message_using_rsa(b'9A74FD04651F460D9552B86142106282BDBBF27FBA834B865BB0D26CED55E3A8')
# print("Encrypted master key", enc_masterkey.hex(), type(enc_masterkey))
# print("Encrypted ephemeral key", enc_ephemeral_private_key.hex())

# enc_ephemeral_key = Helpers.encrypt_message_using_rsa(ephemeral_private_key_string)
# enc_message = fraud_verification_instance.testEncryption(bytes.fromhex('55623A501F228664BE93191A4FAFBC4670B09D45FCAA290200270EF26CD4B576'), ephemeral_private_key_string)
# ciphertext_array = Helpers.convert_hex_into_hex_array(enc_message, 32)
# print("\nCiphertext array", ciphertext_array)
# fraud_verification_instance.verify_fraud(b'55623A501F228664BE93191A4FAFBC4670B09D45FCAA290200270EF26CD4B576', b'9A74FD04651F460D9552B86142106282BDBBF27FBA834B865BB0D26CED55E3A8')
# commit = key_theft_instance.create_commitment('434c4e3174bd56e488f56ce70893e429f0291f4b23fffc0ba1b1840849ea1ba127ad85257ee1c9e978e9483826b5a91df26d33f91009012385cd2337e3f334349bc25bf80b81c39f4591fe301ec0c4d0236b6378e313262e1816a322b9c35da9f27a75e999ef679410f981c2b721f50b158aa7457a2df334775b155afd6984a2',
#     '17c3e83d76de3fe6c662be0542d6560c0e076d2e3cb64bb10e1ba21b8ab804dad1c735f3e8c483b4c297e96a4197f4fe1c08495b76b512255a771537efc50de0a9f45a9f84b94b98865317c78f374438b293ab7067cfe7d6514c42e8c4186b2a96c8de4c437e07120cf9530d02912f2e31309d0dddc795e90220232d9dd1c51f')
# print("commit is: " , commit.hex())
# print(Helpers.convert_string_to_hex_array("LolololLolololLolololLolololLolololLolololLolololLololol"))
# instance = FraudVerificationService.get_instance()
#
# ephemeral_private_key_string = open("E2EKeyTheft_App/Services/ecc_private_key.pem", "r").read()
# fraud_verification_instance.testEncryption(b'55623A501F228664BE93191A4FAFBC4670B09D45FCAA290200270EF26CD4B576', ephemeral_private_key_string)
# print(fraud_verification_instance.get_hkdf_derived_key(b"salt", b"base key", 32))
# print(fraud_verification_instance.get_hkdf_derived_key(b"salt", b"base key", 32))
# print(SHA256.new(b"hello").digest())
# print(SHA256.new(b"hello").digest())
