from Crypto.PublicKey import RSA
from Crypto import Random


class RSAKeyGenerationService:
    publicKey = None
    privateKey = None
    __instance = None

    @staticmethod
    def get_instance():
        if RSAKeyGenerationService.__instance is None:
            return RSAKeyGenerationService()
        return RSAKeyGenerationService.__instance

    def __init__(self):
        print("INIT RSAKeyGenerationService")
        if RSAKeyGenerationService.__instance is not None:
            raise Exception("Singleton instance already exists. Use PublicLeaksService.get_instance() to get that instance.")
        random_generator = Random.new().read
        key = RSA.generate(1024, random_generator)
        self.privateKey = key
        self.publicKey = key.publickey()
        RSAKeyGenerationService.__instance = self



