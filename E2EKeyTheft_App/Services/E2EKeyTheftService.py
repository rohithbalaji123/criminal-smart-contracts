import json
from web3 import Web3

from E2EKeyTheft_App import env


class E2EKeyTheftService:
    __instance = None
    CONTRACT_NAME = "E2EKeyTheft"

    @staticmethod
    def get_instance():
        if E2EKeyTheftService.__instance is None:
            return E2EKeyTheftService()
        return E2EKeyTheftService.__instance

    def __init__(self):
        if E2EKeyTheftService.__instance is not None:
            raise Exception("Singleton instance already exists. Use " + self.__class__.__name__ + ".get_instance() to get that instance.")

        self.w3 = Web3(Web3.HTTPProvider(env.GANACHE_BC_URL))
        with open('E2EKeyTheft_App/truffle_contracts/build/contracts/' + E2EKeyTheftService.CONTRACT_NAME + '.json') as abi_definition:
            self.contract_abi = json.load(abi_definition)

        contract_address = self.contract_abi["networks"][env.NETWORK_ID]["address"]
        self.contract = self.w3.eth.contract(address=Web3.toChecksumAddress(contract_address),
                                             abi=self.contract_abi['abi'])
        self.myWalletAddress = self.w3.eth.accounts[0]

        E2EKeyTheftService.__instance = self

    def intend_to_reveal_key_to_contract(self, commit, account_index):
        self.contract.transact({"from": self.w3.eth.accounts[account_index]}).intendToReveal(bytes.fromhex(commit))

    def reveal_key_to_contract(self, encrypted_master_secret, encrypted_ephemeral_private_key, account_index):
        self.contract.transact({"from": self.w3.eth.accounts[account_index]}).revealKeyToContractor(
            encrypted_master_secret, encrypted_ephemeral_private_key)

    def get_reward_from_contract(self, account_index):
        self.contract.transact({"from": self.w3.eth.accounts[account_index]}).claimReward()

    def retract_reward_from_contract(self):
        self.contract.transact({"from": self.w3.eth.accounts[0]}).retractReward()

    def get_active_perpetrator_address_from_contract(self):
        perpetrator = self.contract.call().getActivePerpetratorAddress()
        return perpetrator

    def get_active_encrypted_master_secret(self):
        active_encrypted_master_secret = self.contract.call().getActiveEncryptedMasterSecret()
        return(active_encrypted_master_secret)

    def get_active_encrypted_ephemeral_private_key(self):
        active_encrypted_ephemeral_private_key = self.contract.call().getActiveEncryptedEphemeralPrivateKey()
        return(active_encrypted_ephemeral_private_key)


    def create_commitment(self, enc_master_key, enc_ephemeral_key):
        return self.contract.call().testFunction(enc_master_key, enc_ephemeral_key)