import json

from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto.PublicKey import ECC
from Crypto.Signature import DSS
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from web3 import Web3

from E2EKeyTheft_App import env
from E2EKeyTheft_App.Helpers import Helpers
from E2EKeyTheft_App.Services.RSAKeyGenerationService import RSAKeyGenerationService
from E2EKeyTheft_App.Helpers import Helpers

class FraudVerificationService:
    __instance = None
    CONTRACT_NAME = "E2EKeyTheft"
    keyGenerationService = RSAKeyGenerationService.get_instance()

    @staticmethod
    def get_instance():
        if FraudVerificationService.__instance is None:
            return FraudVerificationService()
        return FraudVerificationService.__instance

    def __init__(self):
        if FraudVerificationService.__instance is not None:
            raise Exception(
                "Singleton instance already exists. Use " + self.__class__.__name__ + ".get_instance() to get that instance.")

        self.w3 = Web3(Web3.HTTPProvider(env.GANACHE_BC_URL))
        with open('E2EKeyTheft_App/truffle_contracts/build/contracts/' + FraudVerificationService.CONTRACT_NAME + '.json') as abi_definition:
            self.contract_abi = json.load(abi_definition)

        contract_address = self.contract_abi["networks"][env.NETWORK_ID]['address']
        self.contract = self.w3.eth.contract(address=Web3.toChecksumAddress(contract_address),
                                             abi=self.contract_abi['abi'])
        self.myWalletAddress = self.w3.eth.accounts[0]

        FraudVerificationService.__instance = self

    def get_active_perpetrator_address(self):
        perpetratorAddress = self.contract.call().getActivePerpetratorAddress()
        print(perpetratorAddress)
        return perpetratorAddress

    def get_active_encrypted_master_key(self):
        encryptedMasterSecret = self.contract.call().getActiveEncryptedMasterSecret()
        print(encryptedMasterSecret)
        return encryptedMasterSecret

    def get_active_encrypted_ephemeral_private_key(self):
        encryptedEphemeralPrivateKey = self.contract.call().getActiveEncryptedEphemeralPrivateKey()
        print(encryptedEphemeralPrivateKey)
        return encryptedEphemeralPrivateKey

    def get_level(self):
        level = self.contract.call().getLevel()
        return level

    def get_hkdf_derived_key(self, salt, base_key, length):
        backend = default_backend()
        salt = salt
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=length,
            salt=salt,
            iterations=100000,
            backend=backend
        )
        # master_key has to be converted to bytes type
        derived_key = kdf.derive(base_key)
        return derived_key

    def get_ephemeral_public_key_first_victim(self):
        victimOneEphemeralPublicKey = self.contract.call().getVictimOneEphemeralPublicKey()
        return victimOneEphemeralPublicKey

    def get_message(self):
        message = self.contract.call().getMessage()
        message = Helpers.convert_bytes_array_to_string(message)
        return message

    def get_cipher_text(self):
        cipherText = self.contract.call().getCipherText()
        return cipherText

    def verify_fraud(self, master_key, ephemeral_private_key):
        # enc_master_key = Helpers.encrypt_message_using_rsa(master_key)
        # enc_ephemeral_private_key = Helpers.encrypt_message_using_rsa(ephemeral_private_key)
        # print("\n Inside verify Fraud")
        # print("\n Encrypted ephemeral_private key", enc_ephemeral_private_key.hex())
        # print("\n Encrypted master key", enc_master_key.hex())
        # print("\n Encrypted ephemeral_private key from contract ", self.get_active_encrypted_ephemeral_private_key())
        # print("\n Encrypted master key from contract", self.get_active_encrypted_master_key())


        # perpetrator = self.get_active_perpetrator_address()
        # if enc_master_key == self.get_active_encrypted_master_key() and enc_ephemeral_private_key == self.get_active_encrypted_ephemeral_private_key():
        root_key = self.get_hkdf_derived_key(b'0x01', master_key, 32)
        chain_key = self.get_hkdf_derived_key(b'0x02', master_key, 32)
        level = self.get_level()

        assert level > 0, "Invalid value for level"

        for i in range(level):
            message_key = self.get_hkdf_derived_key(b'0x01', chain_key, 32)

            # chain_key has to be converted to bytes type
            chain_key = self.get_hkdf_derived_key(b'0x02', chain_key, 32)
            ephemeral_private_key = open("E2EKeyTheft_App/Services/ecc_private_key.pem", "r").read()
            key = ECC.import_key(ephemeral_private_key)
            signer = DSS.new(key, 'deterministic-rfc6979')
            hash_message = SHA256.new(self.get_ephemeral_public_key_first_victim())
            ephemeral_secret = signer.sign(hash_message)

            chain_root_key = self.get_hkdf_derived_key(b'0x02', root_key + ephemeral_secret, 64)
            chain_key = chain_root_key[:32]
            root_key = chain_root_key[32:]

        # symmetric encryption of message using 32 bytes message key
        enc_suite = AES.new(message_key, AES.MODE_CBC, iv=b"1234567890123456")
        # message to be encrypted has to be a multiple of 16 in length
        message = self.get_message()
        enc_message = enc_suite.encrypt(Helpers.add_cbc_encrypt_padding(message).encode('utf-8'))
        enc_message_array = Helpers.convert_hex_into_byte_array(enc_message.hex(), 32)
        if not Helpers.compare_cipher_texts(enc_message_array, self.get_cipher_text()):
            self.contract.transact({"from": self.w3.eth.accounts[0]}).reportFraud()
            print("Fraud reported.")
        else:
            print("Submitted keys by perpetrator are valid.")

    def testEncryption(self, master_key, ephemeral_private_key):
        root_key = self.get_hkdf_derived_key(b'0x01', master_key, 32)
        chain_key = self.get_hkdf_derived_key(b'0x02', master_key, 32)
        level = self.get_level()

        assert level > 0, "Invalid value for level"

        for i in range(level):
            message_key = self.get_hkdf_derived_key(b'0x01', chain_key, 32)

            # chain_key has to be converted to bytes type
            chain_key = self.get_hkdf_derived_key(b'0x02', chain_key, 32)

            key = ECC.import_key(ephemeral_private_key)
            signer = DSS.new(key, 'deterministic-rfc6979')
            hash_message = SHA256.new(self.get_ephemeral_public_key_first_victim())
            ephemeral_secret = signer.sign(hash_message)

            chain_root_key = self.get_hkdf_derived_key(b'0x02', root_key + ephemeral_secret, 64)
            chain_key = chain_root_key[:32]
            root_key = chain_root_key[32:]

        # symmetric encryption of message using 32 bytes message key
        enc_suite = AES.new(message_key, AES.MODE_CBC, iv=b"1234567890123456")
        print("Message key inside test_encryption", message_key)
        # message to be encrypted has to be a multiple of 16 in length
        message = self.get_message()
        print("Message is: ", message)
        mess = Helpers.add_cbc_encrypt_padding(message).encode('utf-8')
        enc_message = enc_suite.encrypt(mess)
        # enc_suite = AES.new(message_key, AES.MODE_CBC, iv=b"1234567890123456")
        # dec_message = enc_suite.decrypt(enc_message)
        print("\nciphertext is:", enc_message)
        return (enc_message.hex())

    
