Test cases for publicLeaks

test_CreateContract(uint16 _numSegments, uint16 _numPublicSegments, bytes32[] memory _commitment, bytes32[] memory _ciphertexts, uint _startTime, uint _deadline) -> test whether a block is added in ganache-cli
Input => (5, 2, [
		c4aaffe06482fb151b7956c3ad532ff923f5095d67ba1b83d5c9980b60d9fe13,
		25c20722583e2646508ff8dd17ee82a23dbce2e1ef7ab215e236bea57e3d8570,
		e0c1c7708125d7fe8a0ff7255b4e1926cda34f48b321c080ea6d2af3f219de00,
		bef3cda5baca5c2a99e920a34629602421e595a1a77c889a724ec6fbfff66417,
		b8621b0d8a0e54c6641aae178064c4244a70ef38d963383b33ea027fcda675fb
	], [
		85f91ccaca1d27aac98dc7378b6325e1b43de9bc15c6827d227e67a9c40bda08,
		a8b9432d95ec23c3cb2f8be6f6cb66a6bdccff6ab19b0c5114cbf9b2628f3ca2,
		92c306a592125f40010cd9054b56cc5c29c7b0ab488fa6316b91782f23e6d4c5,
		03084c03b878b8e552be08489c6c13a461a08aade2f8be69a48a2be00bdd55a8,
		b358adc77c005a435efaee5c3de9e013f10eb99a36cd89c313ef007a26466768
	], 1552040500, 1552040990)
Output => The block being added to the blockchain



test_getChallenge() - call getChallenge multiple number of times and see the segements set is always same
Input => nil
Output => [2, 4]


test_leakPublicSegments() - assert that state == CONFIRMED
Input => [
	142e5135a19eee116b39086a1e580b08df618c7faebf0827265f1e6be7abdb18,
	b967b07b32b45e78c937f7484810929ff8299b75a7d134a5610d1864c0177d57
]
Output => The keys for the challenged public segments is leaked


test_donate() - get balance amount of donor and contract accounts before call to donate() and get balances after the donate() call . Check the difference
Input => nil
Output => Check if the amount has been donated to the contract


test_leakAllSegments() - assert that hashes of all the input keys is equal to the initial commitment
Input => [
	d3b850801b4a31852f0904fa8d6549e6ec986d292239dfdc9d104f542e17cb26,
	e25f179bf1b1327f1bfcea37b6220c9ad61a1ecac392a225faf036dcafb068ba,
	fd7e05d19d77e532c779d10d09d8cefc8f91dd22cccb02aac28776f0c4234475,
	cab2ec05ae6bc827a0c597f9fd8ebd8f7b3a5733ec7a9bfe05008bc5e2032e76,
	ca35067e7f21295825c71dd738ee0e370d19c0c38acbaea969730518d895c8ce
]
Output => nil

test_retractDonations() - check the difference between the balances of addressIndices[i] before and after, it should be = addressIndices[donor] 
Input => nil
Output => nil






















TEST CASES FOR KEY THEFT:

test_createContract()
Input => (
	ec31803e698f27378634765670845bb55d140085d1e49336e3e729b45e90f7eb,
	1552056452,
	5000
)
Output => The contract being created in blockchain


test_intendToReveal()
Input => 3064a679c4e264c61ed71b8e4ff80155f1e52d20b2a30c2b8bb518468b4f1a07
Output => nil


test_revealKeyToContractor()
Input => (
	00c82d2ef0b0f651cef508d53c50dec116810e968ba4380c4b72bcc348d612de,
	7cc50d3bbbd5facba42c9de744397ea957440700b437acdb4f4007f0d38e5c33,
	3429387
)
Output => nil


test_reportRevocation()
Input => nil
Output => nil


test_getReward()
Input => nil
Output => nil


test_retractReward()
Input => nil
Output => nil