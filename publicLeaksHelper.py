from Crypto.Cipher import AES
from Crypto.Hash import keccak
from web3 import Web3
from Crypto import Random
import time


def generate_data_segments(data, n):
    # data_in_binary = bin(int.from_bytes(data.encode(), 'big'))
    pass


def generate_secret_keys():
    pass


def generate_ciphertexts(data_segments, secret_keys):
    cipher_texts = []
    for i in range(len(data_segments)):
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(secret_keys[i], AES.MODE_CBC, iv)
        print(data_segments[i])
        print(cipher.encrypt(data_segments[i]))
        cipher_texts.append((iv + cipher.encrypt(data_segments[i])).hex())
    return cipher_texts


def generate_secret_key_hashes_with_nonce(secret_keys):
    hashes = []
    for i in range(len(secret_keys)):
        # TODO: append the segment number to the secret key
        keccak_hash = keccak.new(digest_bits=256)
        keccak_hash.update(secret_keys[i])
        key_hash = keccak_hash.hexdigest()

        hashes.append(key_hash)
    return hashes


# number_of_segments = input()
# data_to_be_leaked = input()
# password = input()

# data_segments = generate_data_segments(data_to_be_leaked, number_of_segments)
data_segments = [
    b"0101001101100001",
    b"0110110101110000",
    b"0110110001100101",
    b"0010000001110100",
    b"0110010101110011",
    b"0111010010000000",
]
# secret_keys = generate_secret_keys(password)
secret_keys = [
    bytes.fromhex("428100483DB713B5598FD72C9BE513DF81785ACD358311E9D67161DFBC35323E"),
    bytes.fromhex("8082CACC31741DF1846744C33ABA86A64B690CC717446714468EAC1591DACC76"),
    bytes.fromhex("E34304376B96A2FD1642A52F0791BFC5AD1B0C80F9559534FADEE110BCB4DCDB"),
    bytes.fromhex("47C063EDB1FB85CA5C5676A7CC79FD1DBE8A6BC788A3FB44490A814DE0E94D51"),
    bytes.fromhex("55623A501F228664BE93191A4FAFBC4670B09D45FCAA290200270EF26CD4B576"),
    bytes.fromhex("9A74FD04651F460D9552B86142106282BDBBF27FBA834B865BB0D26CED55E3A8"),
]
number_of_segments = 6
number_of_public_segments = 2
ciphertexts = generate_ciphertexts(data_segments, secret_keys)
commitment = generate_secret_key_hashes_with_nonce(secret_keys)

print(number_of_segments)
print(number_of_public_segments)

print('[', end="")
list(map(lambda x: print('\"0x'+str(x)+'\",', end=""), commitment))
print('\b]')

print('[', end="")
list(map(lambda x: print('\"0x'+str(x)+'\",', end=""), ciphertexts))
print('\b]')

print(int(time.time()))
print(int(time.time()) + 50000)
