### Overview

- Implementation of "Criminal Smart Contract for private key theft in end-to-end encrypted applications"(self-written) paper. 
- Implementation of Public Leaks Smart Contracts devised in "The Ring of Gyges: Using Smart Contracts for Crime" paper.