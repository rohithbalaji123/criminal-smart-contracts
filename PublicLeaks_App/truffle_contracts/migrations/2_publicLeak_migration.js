const PublicLeaks = artifacts.require("PublicLeaks");


module.exports = function(deployer) {
  deployer.deploy(
    PublicLeaks,
    6,
    2,
    [
      "0xd9b90878668922e00a00836e2e3c25b04ad0cca311a00184b71858f78dfd6929",
      "0x511c93a6b0bb69466b4e7d75c20ce47694c1e79fff64e2f71074b2897847b143",
      "0x90219536c902ca01af7e29552a3890e761da9c070ceea1a85a644919cd504820",
      "0xccc964ef44e6ce2bee06150ce0ca44cece747267959567c5dc7105613910acf5",
      "0xd393ae04a84054f10d7781f3c8770a72b13f5683a4b6103303484fcdd4c5b9a5",
      "0xdba856ab76a791a473be7aa892193b5ff5f505b97fe75cc978f98f8bb7fb38d3"
    ],
    [
      "0x2a3b3a45c7a49e429f57c8e33eb52fc82ae931cc79aa10594608ad7d676fba8a",
      "0x523852536b419b2fb4daa93dea03ab2c8ab345f2c0b6ed4a8da3902a8f9f83f3",
      "0xa004500fb5890400c1640942befb963e146cebf9fbe6a761af6ec4e7b30131c8",
      "0xc90ec2eb708e7fb21433244203699d31937fcfdb8ad25b1df0875f7c2ff94078",
      "0x2c7ef6e3f082beda23d6435833d0cc633a0b53119dd9942a014fc295e3af5e99",
      "0x827b215dec8a75680ede9c761dfbd6db4db07ab45c14989be6418b77305d9696"
    ],
    Math.floor(Date.now() / 1000) - 50,
    Math.floor(Date.now() / 1000) + 604750);
};
