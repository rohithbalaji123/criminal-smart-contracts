pragma solidity >=0.4.21 <0.6.0;

contract PublicLeaks {

    enum State {INIT, CREATED, CONFIRMED, ABORTED, COMPLETED}
    State state;
    uint16 numSegments;
    uint16 numPublicSegments;
    bytes32[] publicSegmentsKeys;
    bytes32[] commitment;
    bytes32 [] ciphertexts;
    address payable public seller;
    uint16 [] public challenge;
    mapping(address => uint256) public donations;
    mapping(uint256 => bool) public randomSegmentNumbersMap;
    uint256 donatedAmount;

    address payable[] public donorAddresses;
    bytes32[] masterSecretKey;
    uint startTime;
    uint deadline; //Date are stored in the Unix epoch format (timestamp) which consists in an integer of the number of seconds since 1970-01-01
    // in Javascript we can calculate timestamp as Math.floor(Date.now() / 1000)

    /**
     * While constructing the a contract, following details are feeded as inputs
     * total number of segments,
     * number of segments to be revealed to public initially,
     * list of hashes of keys of each segment,
     * list of encrypted segments,
     * start time of the contract,
     * the end time before which donations should be made.
     */
    constructor(uint16 _numSegments, uint16 _numPublicSegments, bytes32[] memory _commitment, bytes32[] memory _ciphertexts, uint _startTime, uint _deadline) public {

        // Add require for validation
        state = State.CREATED;
        seller = msg.sender;
        numSegments = _numSegments;
        numPublicSegments = _numPublicSegments;
        commitment = _commitment;
        ciphertexts = _ciphertexts;
        startTime = _startTime;
        deadline = _deadline;
    }

    function getContractParams() public view returns (address, uint16, uint16, bytes32[] memory, bytes32[] memory, uint, uint) {

        return (seller, numSegments, numPublicSegments, commitment, ciphertexts, startTime, deadline);
    }

    // Returns the random set of segments to be leaked to public for enticing and store the set in challenge
    function getChallenge() public returns (uint16 [] memory) {
        // Check if the contract has started
        require(block.timestamp > startTime, "Contract hasn't started.");


        uint256 headBlockNumber;
        bytes32 headBlockHash;

        // TODO: Should be changed to the block number of the latest block which got generated before the start time
        headBlockNumber = block.number - 1;
        headBlockHash = blockhash(headBlockNumber);
        // This while condition will handle the case of getChallenge being called for second time
        uint nonce = 0;
        while (challenge.length < numPublicSegments) {
            uint256 randomSegment = uint256(keccak256(abi.encodePacked(headBlockHash, nonce))) % numSegments;
            if (!randomSegmentNumbersMap[randomSegment]) {
                randomSegmentNumbersMap[randomSegment] = true;
                challenge.push(uint16(randomSegment));
            }

            nonce++;
        }
        return challenge;
    }

    function getMasterSecretKey() public view returns (bytes32[] memory msk) {
        require(state == State.COMPLETED);
        return masterSecretKey;
    }

    // returns the decrypted version of all the public segments
    function getLeakedPublicSegmentsKeys() public view returns (bytes32[] memory) {
        require(state == State.CONFIRMED);
        return publicSegmentsKeys;
    }

    function leakPublicSegments(bytes32[] memory _publicSegmentsKeys) public {
        require(msg.sender == seller, "Sender not authorized");
        require(_publicSegmentsKeys.length == numPublicSegments, "publicSegmentsKey length not sufficient");
        require(challenge.length == numPublicSegments, "challenge length not matched");
        require(state == State.CREATED, "state not CREATED.");

        // Check if the hashes of theses keys are equal to the corresponding segments from the commitment
        for (uint i = 0; i < numPublicSegments; i++) {
            require(keccak256(abi.encodePacked(_publicSegmentsKeys[i])) == commitment[challenge[i]], "commitment mismatch.");
            publicSegmentsKeys.push(_publicSegmentsKeys[i]);
        }

        state = State.CONFIRMED;
    }

    function donate() public payable returns (uint256) {
        require(state == State.CONFIRMED);
        address payable donor = msg.sender;
        uint256 amount = msg.value;

        if (donations[donor] == 0 && amount != 0) {
            donorAddresses.push(donor);
        }
        donations[donor] += msg.value;
        donatedAmount += msg.value;
    }

    function getDonatedAmount() public view returns (uint256 totalDonatedAmount) {
        require(state == State.CONFIRMED);
        return donatedAmount;
    }

    function leakAllSegments(bytes32[] memory _privateSegmentKeys) public {
        require(state == State.CONFIRMED);
        require(msg.sender == seller);
        require(_privateSegmentKeys.length == numSegments);

        for (uint i = 0; i < numSegments; i++) {
            require(keccak256(abi.encodePacked(_privateSegmentKeys[i])) == commitment[i]);
            masterSecretKey.push(_privateSegmentKeys[i]);
        }
        seller.transfer(getDonatedAmount());
        state = State.COMPLETED;
    }

    function retractDonations() public {
        require(state != State.COMPLETED, "state is completed");
        require(block.timestamp > deadline, "deadline not over");

        for (uint i = 0; i < donorAddresses.length; i++) {
            donorAddresses[i].transfer(donations[donorAddresses[i]]);
            donatedAmount -= donations[donorAddresses[i]];
        }

        state = State.ABORTED;
    }
}
