import sys

from PublicLeaks_App.Services.PublicLeaksService import PublicLeaksService

mode = sys.argv[1]
instance = PublicLeaksService.get_instance()

print("Getting deployed PublicLeaks contract parameters...")
instance.get_contract_params()

if mode == "contractor":

    while True:
        print("-----------------------------------------------------------------------------------------------------------------")
        print("-----------------------------------------------------------------------------------------------------------------")
        print("Contractor options...")
        print("1. Get challenge from the contract for the seller")
        print("2. Leak public segment keys to the contract")
        print("3. Get current donated amount from the contract")
        print("4. Leak all segment keys to the contract")
        print("5. Exit")

        option = eval(input("Enter your option : "))

        # print()
        if option == 1:
            print("Getting challenge from the contract for the seller:")
            instance.get_challenge_from_contract()
        elif option == 2:
            print("Seller Leaking public segment keys to the contract:")
            for i in range(instance.num_public_segments):
                key = input("Enter key for public segment: ")
                instance.public_segment_keys.append(bytes.fromhex(key))
            instance.leak_public_segments_to_contract(instance.public_segment_keys)
        elif option == 3:
            print("Getting current donated amount from the contract:")
            instance.get_donated_amount_from_contract()
        elif option == 4:
            print("Seller Leaking all segment keys to the contract:")
            for i in range(instance.num_segments):
                key = input("Enter secret key: ")
                instance.segment_keys.append(bytes.fromhex(key))
            instance.leak_all_segments_to_contract(instance.segment_keys)
        elif option == 5:
            quit()
        else:
            print("Invalid option. Retry!")


elif mode == "donor":

    while True:
        print("-----------------------------------------------------------------------------------------------------------------")
        print("-----------------------------------------------------------------------------------------------------------------")
        print("Donor options...")
        print("1. Get leaked public segment keys from the contract")
        print("2. Get current donated amount from the contract")
        print("3. Donate amount to contract")
        print("4. Get master key after the leak")
        print("5. Get my balance")
        print("6. Exit")
       

        option = eval(input("Enter your option : "))

        print()
        if option == 1:
            print("Getting leaked public segment keys from the contract:")
            instance.get_leaked_public_segments_keys_from_contract()
        elif option == 2:
            print("Getting current donated amount from the contract:")
            instance.get_donated_amount_from_contract()
        elif option == 3:
            print("Donating from donor to contract:")
            donor_index = eval(input("Enter account index of donor: "))
            donation = eval(input("Enter donation amount in wei: "))
            instance.donate_to_contract(donor_index, donation)
        elif option == 4:
            print("Getting master key after the leak:")
            instance.get_master_secret_key_from_contract()
        elif option == 5:
            index = eval(input("Enter index of your account: "))
            instance.get_my_balance(index)
        elif option == 6:
            quit()
        else:
            print("Invalid option. Retry!")

    # TODO: decrypt all segments for donor
