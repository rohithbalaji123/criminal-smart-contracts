from PublicLeaks_App import env
import json
import time

from web3 import Web3


class PublicLeaksService:
    __instance = None
    URL = env.GANACHE_BC_URL
    CONTRACT_NAME = "PublicLeaks"
    challenge = None
    num_segments = None
    num_public_segments = None
    seller = None
    public_segment_keys = []
    segment_keys = []

    @staticmethod
    def get_instance():
        if PublicLeaksService.__instance is None:
            return PublicLeaksService()
        return PublicLeaksService.__instance

    def __init__(self):
        if PublicLeaksService.__instance is not None:
            raise Exception("Singleton instance already exists. Use PublicLeaksService.get_instance() to get that instance.")

        self.w3 = Web3(Web3.HTTPProvider(PublicLeaksService.URL))

        with open('PublicLeaks_App/truffle_contracts/build/contracts/' + PublicLeaksService.CONTRACT_NAME + '.json') as abi_definition:
            self.contract_abi = json.load(abi_definition)

        contract_address = self.contract_abi["networks"][env.NETWORK_ID]['address']
        self.contract = self.w3.eth.contract(address=Web3.toChecksumAddress(contract_address),
                                             abi=self.contract_abi['abi'])
        self.myWalletAddress = self.w3.eth.accounts[0]

        PublicLeaksService.__instance = self

    def get_challenge_from_contract(self):
        self.contract.transact({"from": self.myWalletAddress}).getChallenge()
        PublicLeaksService.challenge = self.contract.call().getChallenge()
        print("\n Challenge: ", PublicLeaksService.challenge)

    def leak_public_segments_to_contract(self, public_segment_keys):
        self.contract.transact({"from": self.myWalletAddress}).leakPublicSegments(public_segment_keys)

    def get_leaked_public_segments_keys_from_contract(self):
        publicSegmentKeys = self.contract.call().getLeakedPublicSegmentsKeys()
        print("\n Public segment Keys are:\n")
        for i in publicSegmentKeys:
            print(i.hex())

    def donate_to_contract(self, account_index, donation):
        self.contract.transact({"from": self.w3.eth.accounts[account_index], "value": donation}).donate()

    def get_donated_amount_from_contract(self):
        amount = self.contract.call().getDonatedAmount()
        print("\n Donated Amount: ")
        print(amount)

    def leak_all_segments_to_contract(self, segment_keys):
        self.contract.transact({"from": self.myWalletAddress}).leakAllSegments(segment_keys)

    def get_master_secret_key_from_contract(self):
        msk = self.contract.call().getMasterSecretKey()
        print("\n Master Secret key: \n")
        for i in msk:
            print(i.hex())

    def retract_donations_from_contract(self):
        self.contract.transact({"from": self.w3.eth.accounts[1]}).retractDonations()

    def get_my_balance(self, account_index):
        balance = self.w3.eth.getBalance(self.w3.eth.accounts[account_index])
        print("\nBalance amount : ", balance)

    def get_contract_params(self):
        (seller, num_segments, num_public_segments, commitment, cipherTexts, start_time,
         end_time) = self.contract.call().getContractParams()
        PublicLeaksService.seller = seller
        PublicLeaksService.num_segments = num_segments
        PublicLeaksService.num_public_segments = num_public_segments
        print("\nSeller of public leaks is: ", seller)
        print("Number of segments: ", num_segments)
        print("Number of public segments: ", num_public_segments)
        print("\nCommitment:")
        for i in commitment:
            print(i.hex())
        print("\nCipherTexts:")
        for i in cipherTexts:
            print(i.hex())
        print("\nStart time: ", time.ctime(start_time))
        print("Deadline: ", time.ctime(end_time))
