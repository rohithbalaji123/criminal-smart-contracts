import unittest

from E2EKeyTheft_App.Services.E2EKeyTheftService import E2EKeyTheftService
from E2EKeyTheft_App.Services.FraudVerificationService import FraudVerificationService


class E2EKeyTheftTest(unittest.TestCase):

    e2e_inst = E2EKeyTheftService.get_instance()
    fraud_inst = FraudVerificationService.get_instance()

    def test_init(self):
        self.assertIsInstance(self.e2e_inst, E2EKeyTheftService)
        self.assertIsInstance(self.fraud_inst,FraudVerificationService)

    def test_fraud_functions(self):
        self.assertIsNotNone(self.e2e_inst.contract)
        self.assertIsNotNone(self.fraud_inst.keyGenerationService)


if __name__ == '__main__':
    unittest.main()
