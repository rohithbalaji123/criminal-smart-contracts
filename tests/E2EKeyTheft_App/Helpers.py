import base64

from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from eth_keys import keys
from eth_utils import decode_hex


class Helpers:
    @staticmethod
    def convert_hex_into_byte_array(hex_message, element_length_in_bytes):
        hex_element_length = 2 * element_length_in_bytes
        return [bytes.fromhex(hex_message[i:i + hex_element_length]) for i in
                range(0, len(hex_message), hex_element_length)]

    @staticmethod
    def convert_hex_into_hex_array(hex_message, element_length_in_bytes):
        hex_element_length = 2 * element_length_in_bytes
        return ["0x" + hex_message[i:i + hex_element_length] for i in
                range(0, len(hex_message), hex_element_length)]

    @staticmethod
    def convert_string_to_byte_array(message):
        HEX_ARRAY_ELEMENT_LENGTH = 64

        hex_encoded_message = message.encode('utf-8').hex()
        length_to_be_padded = HEX_ARRAY_ELEMENT_LENGTH - (len(hex_encoded_message) % HEX_ARRAY_ELEMENT_LENGTH)
        hex_encoded_message_with_padding = hex_encoded_message + "8" + "0" * (length_to_be_padded - 1)

        hex_array = Helpers.convert_hex_into_hex_array(
            hex_encoded_message_with_padding,
            HEX_ARRAY_ELEMENT_LENGTH // 2
        )
        return hex_array

    @staticmethod
    def convert_bytes_array_to_string(bytes_array):
        message = ""
        bytes_array[-1] = Helpers.remove_byte_padding(bytes_array[-1])
        for byte_element in bytes_array:
            message += str(byte_element, "utf-8")
        return message

    @staticmethod
    def remove_byte_padding(byte_with_padding):
        hex_string = byte_with_padding.hex()

        i = len(hex_string) - 1
        while hex_string[i] != '8':
            i -= 1

        return bytes.fromhex(hex_string[:i])

    @staticmethod
    def add_cbc_encrypt_padding(message):
        BLOCK_SIZE = 16
        return message + (BLOCK_SIZE - len(message) % BLOCK_SIZE) * \
               chr(BLOCK_SIZE - len(message) % BLOCK_SIZE)

    @staticmethod
    def remove_cbc_encrypt_padding(message):
        return message[:-ord(message[len(message) - 1:])]

    @staticmethod
    def encrypt_message(message):

        pass

    @staticmethod
    def get_public_key_from_private_key(private_key_hex):
        private_key_bytes = decode_hex(private_key_hex)
        private_key = keys.PrivateKey(private_key_bytes)
        public_key = private_key.public_key
        print(base64.b64encode(public_key.to_bytes()))
        return public_key.to_bytes()

    @staticmethod
    def encrypt_message_using_rsa(message):
        public_key_string = open("E2EKeyTheft_App/Services/public_key.pem", "r").read()
        public_key = RSA.importKey(public_key_string)

        encryptor = PKCS1_OAEP.new(public_key)
        encrypted = encryptor.encrypt(message)

        return encrypted

    @staticmethod
    def compare_cipher_texts(cipher_text, cipher_text_from_contract):
        for i in range(len(cipher_text) - 1):
            if cipher_text[i] != cipher_text_from_contract[i]:
                return False
        last_index = len(cipher_text) - 1
        for i in range(len(cipher_text[last_index])):
            if cipher_text[last_index][i] != cipher_text_from_contract[last_index][i]:
                return False
        return True

    @staticmethod
    def decrypt_message_using_rsa(message):
        private_key_string = open("E2EKeyTheft_App/Services/private_key.pem", "r").read()
        private_key = RSA.importKey(private_key_string)

        decryptor = PKCS1_OAEP.new(private_key)
        decrypted = decryptor.decrypt(message)

        return decrypted
