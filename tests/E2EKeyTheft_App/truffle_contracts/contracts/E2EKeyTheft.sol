pragma solidity >=0.4.21 <0.6.0;

contract E2EKeyTheft {
    // State of the contract at any point in time
    enum State {INIT, CREATED, AWAIT_VERIFICATION, ABORTED}
    State state;

    // Address of the owner of the contractor
    address payable public contractor;

    // Reward to be paid for the perpetrator who submits the valid key
    uint256 public reward;

    // Set of public keys(ID, Signed Pre, Ephemeral) to identify the victims
    bytes32 public victimOneIDKey;
    bytes32 public victimTwoIDKey;
    bytes32 public victimOneSignedPreKey;
    bytes32 public victimTwoSignedPreKey;
    bytes32 public victimOneEphemeralPublicKey;
    bytes32 public victimTwoEphemeralPublicKey;

    // A message and it's corresponding cipher text from the victims' chat
    bytes32[] public message;
    bytes32[] public cipherText;

    // The level at which the above message exchange happened
    uint16 public level;

    // deadline before which all the commits are to be done
    uint256 public commitDeadline;

    // deadline before which all the claims for the reward can be done
    uint256 public revealKeyDeadline;

    //
    uint256 public deltaTime;

    // map to store all the commits given by the perpetrators
    mapping(address => bytes32) public commitsMap;

    // timestamp at which the current active claim starts for verification by the contractor
    uint256 verificationStartTime;

    // the duration from verificationStartTime within which the contractor should report the
    // validity of the claim submitted by the perpetrator
    uint256 reportDuration;

    struct KeySubmission {
        string encryptedEphemeralKey;
        string encryptedMasterSecret;
        address perpetratorAddress;
    }

    KeySubmission[] revealKeyList;

    constructor (
        bytes32 _victimOneIDKey,
        bytes32 _victimTwoIDKey,
        bytes32 _victimOneSignedPreKey,
        bytes32 _victimTwoSignedPreKey,
        bytes32 _victimOneEphemeralPublicKey,
        bytes32 _victimTwoEphemeralPublicKey,
        bytes32[] memory _message,
        bytes32[] memory _cipherText,
        uint16 _level,
        uint256 _commitDeadline,
        uint256 _revealKeyDeadline,
        uint256 _report_duration
    ) public payable {

        // Assert condition to make sure at least one claim is possible
        require(_commitDeadline >= now, "commit deadline should be greater than current time.");
        require(_commitDeadline + _report_duration <= _revealKeyDeadline, "reveal key deadline should allow at least one claim.");

        state = State.CREATED;
        contractor = msg.sender;
        reward = msg.value;
        victimOneIDKey = _victimOneIDKey;
        victimTwoIDKey = _victimTwoIDKey;
        victimOneSignedPreKey = _victimOneSignedPreKey;
        victimTwoSignedPreKey = _victimTwoSignedPreKey;
        victimOneEphemeralPublicKey = _victimOneEphemeralPublicKey;
        victimTwoEphemeralPublicKey = _victimTwoEphemeralPublicKey;
        message = _message;
        cipherText = _cipherText;
        level = _level;
        commitDeadline = _commitDeadline;
        revealKeyDeadline = _revealKeyDeadline;
        reportDuration = _report_duration;
    }

    function intendToReveal(bytes32 _commit) public {
        require(state == State.CREATED, "Trying to commit with State not created.");
        require(now < commitDeadline, "Deadline for commit is over.");

        // store the commit in a map
        commitsMap[msg.sender] = _commit;
    }


    function revealKeyToContractor(string memory encryptedMasterSecret, string memory encryptedEphemeralKey) public {
        require(block.timestamp > commitDeadline, "Commit Deadline not over yet");
        require(block.timestamp < revealKeyDeadline - (revealKeyList.length * reportDuration), "Deadline to reveal key is over");
        // assert(now() > commitDeadline && now() < revealKeyDeadline - len(revealKeyList)*report_duration + ε);
        // assert(intend exists && intend is valid);
        require(commitsMap[msg.sender] != 0x0, "Commit doesn't exist for the perpetrator.");
        require(keccak256(abi.encodePacked(encryptedMasterSecret, encryptedEphemeralKey)) == commitsMap[msg.sender], "Commit mismatch with the given encrypted keys.");

        KeySubmission memory key;
        key.encryptedEphemeralKey = encryptedEphemeralKey;
        key.encryptedMasterSecret = encryptedMasterSecret;
        key.perpetratorAddress = msg.sender;

        revealKeyList.push(key);

        if (revealKeyList.length == 1) {
            state = State.AWAIT_VERIFICATION;
            verificationStartTime = now;
        }
    }

    function claimReward() public {
        require(state == State.AWAIT_VERIFICATION, "state not equal to await verification");
        require(now > verificationStartTime + reportDuration, "Time to reward hasn't arrived yet");
        require(msg.sender == revealKeyList[0].perpetratorAddress, "The sender is not the current perpetrator");

        msg.sender.transfer(reward);
        state = State.ABORTED;
    }

    function retractReward() public {
        require(state == State.CREATED, "State should be CREATED.");
        require(now > revealKeyDeadline, "endTime hasn't come.");
        // if the state is not in aborted and the time is greater than endTime, then retract the contract and return the amount to the contractor.
        contractor.transfer(reward);
        state = State.ABORTED;
    }

    function reportFraud() public {
        require(revealKeyList.length > 0, "revealKeyList should not be empty.");
        require(state == State.AWAIT_VERIFICATION, "state should be AWAIT_VERIFICATION.");
        require(now < verificationStartTime + reportDuration, "Time to report is over.");

        for (uint i = 0; i < revealKeyList.length - 1; i++) {
            revealKeyList[i] = revealKeyList[i + 1];
        }
        delete revealKeyList[revealKeyList.length - 1];
        revealKeyList.length--;

        if (revealKeyList.length == 0) {
            state = State.CREATED;
        }
    }

    function getActivePerpetratorAddress() view public returns (address) {
        require(state == State.AWAIT_VERIFICATION);
        return revealKeyList[0].perpetratorAddress;
    }

    function getActiveEncryptedMasterSecret() view public returns (string memory) {
        require(state == State.AWAIT_VERIFICATION);
        return revealKeyList[0].encryptedMasterSecret;
    }

    function getActiveEncryptedEphemeralPrivateKey() view public returns (string memory) {
        require(state == State.AWAIT_VERIFICATION);
        return revealKeyList[0].encryptedEphemeralKey;
    }

    function getLevel() view public returns (uint16) {
        return level;
    }

    function getVictimOneEphemeralPublicKey() view public returns (bytes32) {
        return victimOneEphemeralPublicKey;
    }

    function getVictimTwoEphemeralPublicKey() view public returns (bytes32) {
        return victimTwoEphemeralPublicKey;
    }

    function getMessage() view public returns (bytes32[] memory) {
        return message;
    }

    function getCipherText() view public returns (bytes32[] memory) {
        return cipherText;
    }

    function getReward() view public returns (uint256) {
        return reward;
    }

    function testFunction(string memory encryptedMasterSecret, string memory encryptedEphemeralKey) pure public returns (bytes32) {
        bytes32 kec_hash = keccak256(abi.encodePacked(encryptedMasterSecret, encryptedEphemeralKey));
        return kec_hash;
    }
}
