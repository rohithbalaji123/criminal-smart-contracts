import unittest

from PublicLeaks_App.Services.PublicLeaksService import PublicLeaksService


def generate_public_segment_keys():
    return []


def checkContractBalance():
    pass


def get_all_secret_keys():
    pass


def get_keys_from_contract():
    pass


class PublicLeaksTest(unittest.TestCase):

    inst = PublicLeaksService.get_instance()
    challenge = None

    def test_init(self):
        self.assertIsInstance(self.inst, PublicLeaksService)

    def test_challenge_functions(self):
        self.inst.get_challenge_from_contract()
        self.assertTrue(len(self.inst.challenge) > 0)

    def test_get_leaked_public_segments_keys_from_contract(self):
        self.inst.get_leaked_public_segments_keys_from_contract()
        self.assertTrue(len(self.inst.contract.public_segments) > 0)

    def test_leak_public_segments_to_contract(self):
        keys = generate_public_segment_keys()
        self.inst.leak_public_segments_to_contract(keys)

    def test_donate_to_contract(self):
        self.inst.donate_to_contract(1, 5)
        self.assertTrue(checkContractBalance() == 5)

    def test_get_donated_amount_from_contract(self):
        self.inst.donate_to_contract(1, 5)
        self.assertTrue(self.inst.get_donated_amount_from_contract() == 5)

    def test_leak_all_segments_to_contract(self):
        self.inst.leak_all_segments_to_contract(get_all_secret_keys())
        self.assertTrue(get_keys_from_contract() == get_all_secret_keys())

    def test_get_master_secret_key_from_contract(self):
        self.assertTrue(self.inst.get_master_secret_key_from_contract() == get_all_secret_keys())

    def test_get_my_balance(self):
        self.assertTrue(self.inst.get_master_secret_key_from_contract() == get_all_secret_keys())


if __name__ == '__main__':
    unittest.main()
